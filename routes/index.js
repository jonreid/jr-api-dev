const express = require("express");
const router = express.Router();
var walmart = require('walmart')(process.env.WALMART_KEY);
var bestbuy = require('bestbuy')(process.env.BESTBUY_KEY);

/* GET status method. */
router.get("/", function(req, res) {
  res.status(200).send("Welcome, restful API active");
});

/* GET product. */
router.get("/product/:product_name", function(req, res) {
  console.log("Search issued for " + req.params.product_name);
  // searchString = '(search=' + req.params.product_name + ')'
  bestbuyPrice = 0;
  walmartPrice = 0;
  walmartResult = {};
  bestbuyResult = {};
  bestbuy.products('(search=' + req.params.product_name + ')', {show: 'salePrice,name', pageSize: 1}, function(err, data) {
      if (err) console.warn(err);
      else if (data.total === 0) { console.log('No products found at Best Buy');}
      else {
		bestbuyPrice = data.products[0].salePrice;
      	bestbuyResult = {
		    "productName": data.products[0].name,
		    "bestPrice": data.products[0].salePrice,
		    "currency": "CAD",
		    "location": "Best Buy"
		}
      }
    });

  walmart.search(req.params.product_name, "format=json&sort=price&order=asc").then(function(wdata) {
	  if(wdata['totalResults'] == 0) {
	  	console.log('No products found at Walmart');
	  	if(bestbuyPrice == 0) {
	  		//then we have no products to show
	  		res.status(404).send('No products found');
	  	}
	  } 
	  else {
	  	//sort in search is not respected so find our own lowest price
	  	walmartPrice = wdata['items'][0]['salePrice']
	  	walmartResult = {
		    "productName": wdata['items'][0]['name'],
		    "bestPrice": wdata['items'][0]['salePrice'],
		    "currency": "CAD",
		    "location": "Walmart"
		}
	  	for (var i = wdata['items'].length - 1; i >= 0; i--) {
	  		// console.log(wdata['items'][i]['salePrice']);
	  		if (wdata['items'][i]['salePrice'] < walmartPrice) {
			    walmartPrice = wdata['items'][i]['salePrice']; 
			    walmartResult = {
			    "productName": wdata['items'][i]['name'],
			    "bestPrice": wdata['items'][i]['salePrice'],
			    "currency": "CAD",
			    "location": "Walmart"
				}
			}
	  	}

	  	//if we made it this far, at least one of the stores has a product
	  	if(bestbuyPrice < walmartPrice && bestbuyPrice != 0) {
			res.json(bestbuyResult);
		}
		else {
			res.json(walmartResult);
		}

		console.log(bestbuyPrice);
		console.log(walmartPrice);
	  }
  });
});

module.exports = router;
