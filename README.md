Installation
=============
create a .env file in the root directory and add two lines
WALMART_KEY='<your_api_key_here>'
BESTBUY_KEY='<your_api_key_here>'

npm install

node app.js

usage example:
http://localhost:3000/product/ipod returns:
{"productName":"3YR ipod PSP","bestPrice":39.99,"currency":"CAD","location":"Best Buy"}

No results example:
http://localhost:3000/product/xpaladocious returns:
No products found
